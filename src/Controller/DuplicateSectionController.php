<?php

namespace Drupal\section_duplicate\Controller;

use Drupal\Component\Plugin\DerivativeInspectionInterface;
use Drupal\Core\Ajax\AjaxHelperTrait;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\layout_builder\Controller\LayoutRebuildTrait;
use Drupal\layout_builder\LayoutTempstoreRepositoryInterface;
use Drupal\layout_builder\SectionStorageInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Defines a controller to duplicate a section.
 *
 * @internal
 *   Controller classes are internal.
 */
class DuplicateSectionController implements ContainerInjectionInterface {

  use AjaxHelperTrait;
  use LayoutRebuildTrait;

  /**
   * The layout tempstore repository.
   *
   * @var \Drupal\layout_builder\LayoutTempstoreRepositoryInterface
   */
  protected $layoutTempstoreRepository;

  /**
   * DuplicateSectionController constructor.
   *
   * @param \Drupal\layout_builder\LayoutTempstoreRepositoryInterface $layout_tempstore_repository
   *   The layout tempstore repository.
   */
  public function __construct(LayoutTempstoreRepositoryInterface $layout_tempstore_repository) {
    $this->layoutTempstoreRepository = $layout_tempstore_repository;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('layout_builder.tempstore_repository')
    );
  }

  /**
   * Duplicates the section.
   *
   * @param \Drupal\layout_builder\SectionStorageInterface $section_storage
   *   The section storage.
   * @param int $delta
   *   The delta of the section to duplicate.
   *
   * @return \Symfony\Component\HttpFoundation\Response
   *   The controller response.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function duplicate(SectionStorageInterface $section_storage, $delta) {

    $section = $section_storage->getSection($delta);

    $duplicated_section = clone $section;

    // Handle inline blocks.
    foreach ($duplicated_section->getComponents() as $component) {
      $plugin = $component->getPlugin();
      if ($plugin instanceof DerivativeInspectionInterface && $plugin->getBaseId() === 'inline_block') {

        $configuration = $plugin->getConfiguration();

        if (!empty($configuration['block_revision_id'])) {

          $block = \Drupal::entityTypeManager()->getStorage('block_content')->loadRevision($configuration['block_revision_id']);

          $duplicated_block = $block->createDuplicate();

          $configuration['block_serialized'] = serialize($duplicated_block);
          $configuration['block_revision_id'] = NULL;

          $component->setConfiguration($configuration);

        }

      }
    }

    $section_storage->insertSection($delta, $duplicated_section);

    $this->layoutTempstoreRepository->set($section_storage);

    if ($this->isAjax()) {
      return $this->rebuildAndClose($section_storage);
    }
    else {
      $url = $section_storage->getLayoutBuilderUrl();
      return new RedirectResponse($url->setAbsolute()->toString());
    }
  }

}
