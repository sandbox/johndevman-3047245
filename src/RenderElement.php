<?php

namespace Drupal\section_duplicate;

use Drupal\Core\Url;

class RenderElement {

  public static function preRender($element) {

    /** @var \Drupal\layout_builder\SectionStorageInterface $section_storage */
    $section_storage = $element['#section_storage'];

    $storage_type = $section_storage->getStorageType();
    $storage_id = $section_storage->getStorageId();

    foreach ($element['layout_builder'] as &$item) {
      if (isset($item['layout-builder__section'])) {

        $delta = $item['layout-builder__section']['#attributes']['data-layout-delta'];

        $item['duplicate'] = [
          '#type' => 'link',
          '#title' => t('Duplicate section <span class="visually-hidden">@section</span>', ['@section' => $delta + 1]),
          '#url' => Url::fromRoute('section_duplicate.duplicate_section', [
            'section_storage_type' => $storage_type,
            'section_storage' => $storage_id,
            'delta' => $delta,
          ]),
          '#attributes' => [
            'class' => [
              'use-ajax',
              'layout-builder__link',
              'layout-builder__link--duplicate',
            ],
          ],
        ];
      }
    }

    return $element;

  }

}
